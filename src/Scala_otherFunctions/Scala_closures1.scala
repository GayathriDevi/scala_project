package Scala_otherFunctions

object Scala_closures1 {

   def main(args: Array[String]) {
      println( "muliplier(1) value = " +  multiplier(1) )
      println( "muliplier(2) value = " +  multiplier(2) )
   }
   var factor = 8
   val multiplier = (i:Int) => i * factor
}
/*There are two free variables in multiplier: i and factor.
One of them, i, is a formal parameter to the function. 
Hence, it is bound to a new value each time multiplier is called.*/

/*factor has a reference to a variable 
outside the function but in the enclosing scope.*/