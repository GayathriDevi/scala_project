package Scala_otherFunctions

import scala.util.matching.Regex

object Regex_2 {

  def main(args: Array[String]) {
    val pattern = new Regex("(S|s)cala")
    val str = "Scala is object oriented programming and scala is a functionl programming"
    //println((pattern findAllIn str).mkString(","))

    /*We create a String and call the r( ) method on it. Scala implicitly converts 
  the String to a RichString and invokes that method to get an instance of Regex.*/

    val pattern1 = "(S|s)cala".r
    val str1 = "Scala is scalable and cool"
    //println(pattern1 replaceFirstIn(str1, "Java"))
    // println(pattern replaceAllIn(str, "Java"))

    val pattern2 = new Regex("scala[cv]\\d+")
    val str2 = "scalav2 , scalav3, scalac programming language"

    println((pattern2 findAllIn str2).mkString(","))
  }
}


