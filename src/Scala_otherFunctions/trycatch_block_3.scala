package Scala_otherFunctions

import java.io.IOException
import java.io.FileNotFoundException
import java.io.FileReader

object trycatch_block_3 {


   def main(args: Array[String]) {
      try {
         val f = new FileReader("input.txt")
      } catch {
         case ex: FileNotFoundException => {
            println("Missing file exception")
         }
         case ex: IOException => {
            println("IO Exception")
         }
      } finally {
         println("Exiting finally...")
      }
   }
}