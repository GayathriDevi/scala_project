package JDBC_Connectivity

import java.sql.DriverManager
import java.sql.Connection

object jdbc_connectivity {
  
  def main(args: Array[String]) {
    // connect to the database named "mysql" on the localhost
    val driver = "com.mysql.jdbc.Driver"
    val url = "jdbc:mysql://localhost/test"
    val username = "root"
    val password = "root"
 
    // there's probably a better way to do this
    var connection:Connection = null
 
    try {
      // make the connection
      Class.forName(driver)
      connection = DriverManager.getConnection(url, username, password)
 
      // create the statement, and run the select query
      val statement = connection.createStatement()
    
      /*val sql = "update employee SET empcity = 'Pune' WHERE empid in (2,3)";
      statement.executeUpdate(sql);*/
       val resultSet = statement.executeQuery("SELECT * FROM employee")
      while ( resultSet.next() ) {
        val empid = resultSet.getInt("empid")
        val empname = resultSet.getString("empname")
        val empcity = resultSet.getString("empcity")
       println("empid, empname,empcity = " + empid + ", " + empname +","+ empcity +",")
         
      }
    } catch {
      case e => e.printStackTrace
    }
    connection.close()
  }

}