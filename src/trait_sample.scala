trait one{
  def method1()  
}

trait two extends one{
  def method2()
}
trait four {
  def method4=println("Hello")
}
//class three extends one with two with four
class three extends two with four{
  def method3()=println("My class")
  def method1() =println("trait one")
  def method2()=println("trait two")
  
}
object trait_sample {
def main(args: Array[String]): Unit = {
  val t=new three
  t.method1()
  t.method2()
  t.method3()
  t.method4
}
}