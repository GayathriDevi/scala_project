trait white_car{
  val color = "White"
  def speed = println("60/km")
}
trait red_car extends white_car {
  override val color = "Red"
  override def speed = println("120/km")
}
class Levelone extends white_car with red_car {
  
  //override def speed = println("180/km")
}
//the "with" keyword goes between type names for multiple inheritance
//Levelone mixes in red_car after mixes in white_car
class Leveltwo extends red_car{
  
}

object Trait_sam{
  def main(args: Array[String]) {
   
    /*val s=new Levelone
    println(s.color)
    s.speed
    val s1=new Leveltwo
    println(s1.color)
    s1.speed*/
    val s2=new Levelone
    println(s2.color)
    s2.speed
    
  }
}