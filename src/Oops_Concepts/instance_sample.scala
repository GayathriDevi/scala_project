package Oops_Concepts

object instance_sample {
def main(args: Array[String]) {
  val f=new firstclass
  println(f.isInstanceOf[firstclass])
  f.showout
  val s =new secondclass
  println(s.isInstanceOf[secondclass])
  s.printout
  s.asInstanceOf[firstclass]
  println(s.isInstanceOf[firstclass])
  s.showout
  s.printout
  }
  }
class firstclass{
  def showout=println("This is firstclass")
}
class secondclass extends firstclass{
  def printout=println("This is second class")
}