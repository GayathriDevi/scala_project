package Oops_Concepts

object Trait_example {
   def main(args: Array[String]) {
      val p1 = new Pointtt(2, 3)
      val p2 = new Pointtt(2, 4)
      val p3 = new Pointtt(3, 3)

      /*println(p1.isNotEqual(p2))
      println(p1.isNotEqual(p3))
      println(p1.isNotEqual(2))*/
   }
}
  
trait Equal {
  def isEqual(x: Any): Boolean
  def isNotEqual(x: Any): Boolean = !isEqual(x)
}

class Pointtt(xc: Int, yc: Int) extends Equal {
  var x: Int = xc
  var y: Int = yc
  def isEqual(obj: Any) =
    obj.isInstanceOf[Pointtt] &&
    obj.asInstanceOf[Pointtt].x == x
   // println(obj.isInstanceOf[Pointtt])
}