package Oops_Concepts

object Instanceof_example {
  def main(args: Array[String]): Unit = {
    


 class fruit
    class apple extends fruit
    class custard_apple extends apple
   
    val banana = new fruit
    val app = new apple
    val ca = new custard_apple
   
  println("banana.isInstanceOf[fruit]: " + (banana.isInstanceOf[fruit]))  // true
    println("app.isInstanceOf[apple]: " + (app.isInstanceOf[apple]))  // true
    println("ca.isInstanceOf[custard_apple]: " + (ca.isInstanceOf[custard_apple]))  // true

    //println(ca.asInstanceOf[apple])
}
  
  }