package Oops_Concepts

object Instanceof_example1 {
def main(args: Array[String]): Unit = {

val vi =new testinstance
//vi.show(vi)
val ano=new another
//ano.check(vi)
println(ano.isInstanceOf[testinstance])
ano.asInstanceOf[testinstance]
ano.show(vi)
}
    
  }
class testinstance{
    def show(a:Any)
  {
    if (a.isInstanceOf[testinstance])
      println("Instance of")
      else
        println("Not an instance")
  }
  }
class another extends testinstance{
   def check(a:Any)
  {
    if (a.isInstanceOf[testinstance])
      println("Instance of")
      else
        println("Not an instance")
  }
}