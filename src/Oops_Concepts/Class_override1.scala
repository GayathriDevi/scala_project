package Oops_Concepts

import java.io._

class Pointss(val xc: Int, val yc: Int) {
   var x: Int = xc
   var y: Int = yc
   def move(dx: Int, dy: Int) {
      x = x + dx
      y = y + dy
      println ("Point x location : " + x);
      println ("Point y location : " + y);
   }
}

class Locationss(override val xc: Int, override val yc: Int) extends Points(xc, yc){
   
   override def move(dx: Int, dy: Int) {
      x = x - dx
      y = y - dy
    
      println ("Point x location : " + x);
      println ("Point y location : " + y);
     
   }
}

object Class_override1 {
   def main(args: Array[String]) {
      val loc = new Locationss(100, 200);

      // Move to a new location
      loc.move(10, 10);
   }
   
}

