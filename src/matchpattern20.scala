object matchpattern {

   def main(args: Array[String]) {
      println(matchpattern("two"))
      println(matchpattern("test"))
      println(matchpattern(1))
      println(matchpattern(8))

   }
   def matchpattern(x: Any): Any = x match {
      case 1 => "one"
      case "two" => 2
      case y: Int => "scala.Int"
      case _ => "many"
   }
}
