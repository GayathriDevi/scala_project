trait Drawable {
    def draw() { }
}

trait Cowboy extends Drawable {
    override def draw() { println("Bang!") }
}

trait Artist extends Drawable {
    override def draw() { println("A pretty painting") }
}

//class CowboyArtist extends Cowboy with Artist{
  
//}
class CowboyArtist extends Cowboy {
  
}

object Traits8_sample {
  def main(args: Array[String]) {
   val c =new CowboyArtist
   c.draw()
    
  }
}