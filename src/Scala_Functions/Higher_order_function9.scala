package Scala_Functions

object Higher_order_function9 {
  def safeStringOp(s: String, f: String => String) = {
      if (s != null) f(s) else s
      }

def reverser(s: String) = s.reverse

def main(args: Array[String]){

  println(safeStringOp(null, reverser))

  println(safeStringOp("Ready", reverser))
}
   
}
