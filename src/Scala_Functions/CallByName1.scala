package Scala_Functions
object Test {
   def main(args: Array[String]) {
       // delayed(time());
     add(sum());
   }

   /*def time() = {
      println("Getting time in nano seconds")
      System.nanoTime
   }
   def delayed( t: => Long ) = {
      println("In delayed method")
      println("Param: " + t)
      //t
   }*/
   def sum(): Int={
     println("Actual code to do summation")
     return 10+12;
   }
   
   
   def add(s: => Int) ={ //call by name parameter by putting the => symbol 
                         //between the variable name and the type
     println("Addition Block")
     println("Value="+s)
     
   }
}
