package Scala_Functions

object default_parameters3 {
   def main(args: Array[String]) {
        println( "Result : " + addInt());
   }
   def addInt( a:Int=5, b:Int=7 ) : Int = {
      var sum:Int = 0
      sum = a + b

      return sum
   }
}