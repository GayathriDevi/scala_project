package Scala_Functions

object Function_Literal10 {

  def main(args: Array[String]): Unit = {
    def summ (a1:Int, b1:Int):Int = a1+b1
    val add:(Int,Int)=> Int = summ
    println(add(5,2))
    /*val sum = (a : Int ,b :Int)=> a+b
    println(sum(5,2))*/
    
    
  }
}