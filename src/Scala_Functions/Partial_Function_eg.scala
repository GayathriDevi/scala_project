package Scala_Functions

object Partial_Function_eg {
  def main(args: Array[String]): Unit = {
    

def add(a:Int,b:Int)=a+b // A function with two arguments of datatype Int
val addition=add(_:Int,24) // this will not store anything , jus a reference to the 
                            //function
println(addition(10))// pass the value , this gets executed along with the fixed 
                      //argument

}
  }
/*object Partial_Function_eg {
  def main(args: Array[String]) {
    def email(username:String,domain :String)=println(username+domain)
    val emailid=email(_:String,"@gmail.com")
    emailid("gayathri")
    def newemail(username1:String,domain1 :String,domain2:String) = println(username1+domain1+domain2)
    val newemailid=newemail(_:String,"@gmail",_:String)
    newemailid("gayathri",".co.in")
  }   
}*/
