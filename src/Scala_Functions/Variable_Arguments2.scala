package Scala_Functions

object Variable_Arguments2 {
   def main(args: Array[String]) {
       // printStrings("Hello")
        printStrings("Scala", "Python")
        //printStrings("Java","Perl","Scala")
        
   }
   def printStrings( a:String* ) = {
      var i : Int = 0;
      for( b <- a ){
         println("Arg value[" + i + "] = " + b );
         i = i + 1;
         /*println(b)
         println(a)*/
         
      }
   }
}