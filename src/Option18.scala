object Option18 {

  def main(args: Array[String]) {
   // val capitals = Map("France" -> 1, 2-> "Tokyo")
    //val capitals = Map("France" -> "Paris", "Japan" -> "Tokyo")
    val capitals = Map("France" -> 12345, "Japan" -> 78910,"India"-> 111213)
   /* println("show(capitals.get( \"Japan\")) : " +
      show(capitals.get("Japan")))
    println("show(capitals.get( \"India\")) : " +
      show(capitals.get("India")))*/
    
    println("Capital of France  :"+ show("France"))
    println("Capital of Japan :"+ show("Japan"))
    //println("Capital of France  :"+ show(2))
     println("Capital of India  :"+ show("India"))
     println("Capital of Indonesia  :"+ show("Indonesia"))
  }

 /* def show(x: String) : String = x match {
    case "France" => "Paris"
    case "Japan" => "Tokyo"
    case _ => "?"
  }*/
  
 /* def show(x: Any) : Any = x match {
    case "France" => 1
    case 2 => "Tokyo"
    case _ => "?"
  }*/
  
   def show(x:String) : Int = x match {
    case "France" => 12345
    case "Japan" => 78910
    case "India" => 111213
    case _ => 0
  }
}