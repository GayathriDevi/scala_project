
abstract class Product(pname: String, pcode: Int, pdesc: String) {
  def first 

  def second() = println(pname + pcode + pdesc)
}
class Mobile(pname: String, pcode: Int, pdesc: String) extends Product(pname, pcode, pdesc) {

  override def first() {
    println("5000")
  }
}
class Tab(pname: String, pcode: Int, pdesc: String) extends Product(pname, pcode, pdesc) {
  override def first() = println("22000")
}
object Demo extends App {
  val d = new Mobile("Samsung mobile", 34567, "Model No X1234")
  d.first()
  d.second()
  /*val d1 = new Tab("Apple", 390033, "Model No X1YYY3")
  d1.first()
  d1.second()*/

}

