object List11 {

   def main(args: Array[String]) {
     //val lang = "Java" :: ("Scala" :: ("Python" :: Nil))
      val lang :List[String]=List( "Java" ,"Scala","Python","Perl")
      val version:List[Int]=List(12,56,7,8)
      val nums = Nil
      val empty: List[Nothing] = List()
      println( "Head of Languages : " + lang.head)
      println( "Tail of languages : " + lang.tail )
      println("Reverse of List:" + lang.reverse)
      println( "Check if language is empty : " + lang.isEmpty )
      println( "Check if nums is empty : " + nums.isEmpty )
      println(version)
      
      for(i <- lang){ //List operator
        println(i)
        
      }
      
   }
}

