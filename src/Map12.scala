
object map {
def main(args: Array[String]) {
      val colors = Map("red"->"#FF0000", //->Map operator
                       "green" -> "#F0FFFF",
                       "blue" -> "#CD853F"
                       )

      val nums: Map[Int, Int] = Map()
     
      println( "Keys in colors : " + colors.keys )
      println( "Values in colors : " + colors.values )
      println( "Check if colors is empty : " + colors.isEmpty )
      println( "Check if nums is empty : " + nums.isEmpty )
      for(i <- colors)//List operator
      {
        println(i)
      }
   }

}
